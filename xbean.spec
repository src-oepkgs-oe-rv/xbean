Name:          xbean
Version:       4.9
Release:       3
Summary:       A plugin based server
License:       ASL 2.0
URL:           http://geronimo.apache.org/xbean/
Source0:       http://repo2.maven.org/maven2/org/apache/%{name}/%{name}/%{version}/%{name}-%{version}-source-release.zip
Patch0:        0002-Port-to-Eclipse-Luna-OSGi.patch
Patch1:        0003-Port-to-QDox-2.0.patch
BuildArch:     noarch

BuildRequires: maven-local, mvn(commons-logging:commons-logging-api), mvn(log4j:log4j:1.2.12), mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires: mvn(org.apache.maven.plugins:maven-source-plugin), mvn(org.osgi:org.osgi.core), mvn(org.ow2.asm:asm), mvn(org.ow2.asm:asm-commons)
BuildRequires: mvn(org.slf4j:slf4j-api), mvn(org.eclipse:osgi), mvn(org.codehaus.groovy:groovy-all), mvn(ant:ant), mvn(commons-logging:commons-logging)
BuildRequires: mvn(com.thoughtworks.qdox:qdox), mvn(org.apache.maven:maven-archiver), mvn(org.apache.maven:maven-artifact), mvn(org.apache.maven:maven-plugin-api)
BuildRequires: mvn(org.apache.maven:maven-project), mvn(org.apache.maven.plugins:maven-antrun-plugin), mvn(org.apache.maven.plugins:maven-plugin-plugin)
BuildRequires: mvn(org.codehaus.plexus:plexus-archiver), mvn(org.codehaus.plexus:plexus-utils)
Provides:      %{name}-javadoc%{?_isa} %{name}-javadoc
Obsoletes:     %{name}-javadoc

%description
The goal of XBean project is to created a plugin based server analogous to Eclipse being a plugin based IDE.
XBean will be able to discover, download and install server plugins from an Internet based repository.
In addition, we include support for multiple IoC systems, support for running with no IoC system, JMX without
JMX code, lifecycle and class loader management, and a rock solid Spring integration.

%prep
%autosetup -p1
rm -rf src/site/site.xml

%pom_remove_parent
%pom_remove_dep mx4j:mx4j
%pom_remove_dep -r :xbean-finder-shaded
%pom_remove_dep -r :xbean-asm6-shaded

%pom_disable_module xbean-finder-shaded
%pom_disable_module xbean-asm6-shaded
%pom_disable_module xbean-blueprint

%pom_add_dep org.apache.xbean:xbean-asm-util:%{version} xbean-reflect

%pom_xpath_remove 'pom:scope[text()="provided"]' xbean-reflect xbean-asm-util
%pom_xpath_remove pom:optional xbean-reflect xbean-asm-util

sed -i 's/org\.apache\.xbean\.asm6/org.objectweb.asm/g' `find xbean-reflect -name '*.java'`
%pom_remove_dep org.springframework:
%pom_disable_module xbean-classloader
%pom_disable_module xbean-spring
%pom_disable_module maven-xbean-plugin
%pom_remove_plugin :apache-rat-plugin
%pom_remove_plugin :maven-xbean-plugin xbean-classloader

sed -i "s|<Private-Package>|<!--Private-Package>|" xbean-blueprint/pom.xml
sed -i "s|</Private-Package>|</Private-Package-->|" xbean-blueprint/pom.xml

%build
%mvn_build -f

%install
%mvn_install

%files -f .mfiles
%doc NOTICE
%license LICENSE
%dir %{_javadir}/%{name}
%{_javadocdir}/%{name}/*
/usr/share/maven*

%changelog
* Wed Mar 11 2020 yanglijin <yanglijin@huawei.com> - 4.9-3
- modify require

* Mon Dec 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.9-2
- Package init
